#include "ros_image2D_listener.hpp"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>

namespace ImageReceivers
{
Image2DTopic::Image2DTopic(ros::NodeHandle &n, const std::string &topicName)
    {
        this->topicName = topicName;
        this->it = boost::make_shared<image_transport::ImageTransport>(n);
    }

    void 
    Image2DTopic::SubscribeToTopic()
    {
        this->sub = (this->it).get()->subscribe(topicName.c_str(), 1, &ImageReceivers::Image2DTopic::ImageCallback, this);
        std::cout << "Subscribed to ImageTransport topic: " << topicName << std::endl;
    }

    void 
    Image2DTopic::ImageCallback(const sensor_msgs::ImageConstPtr &msg)
    {
        try
        {
            if (msg->encoding == sensor_msgs::image_encodings::RGB8)
                cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            else
                cv_ptr = cv_bridge::toCvCopy(msg, msg->encoding);

            image = cv_ptr->image;
            newImage = true;
        }
        catch (cv_bridge::Exception &e)
        {
            std::cout << "cv_bridge exception" << std::endl;
            ROS_ERROR("cv_bridge exception: %s", e.what());
        }
    }

    bool 
    Image2DTopic::IsNewImageReceived()
    {
        if (newImage)
        {
            newImage = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    cv::Mat
    Image2DTopic::GetImage()
    {
        return image;
    }

    void 
    Image2DTopic::Display()
    {
        std::string title = "Preview of: " + topicName;
        cv::imshow(title.c_str(), image);
    }

    void 
    Image2DTopic::DisplayHistEqualized(double alpha)
    {
        cv::Mat imageHistEq;

        if (image.channels() != 1)
        {
            std::cerr << "Histogram equalization is available only for single channel images." << std::endl;
            return;
        }
        else if (image.type() != 0)
        {
            image.convertTo(imageHistEq, CV_8UC1, alpha);
        }
        else
        {
            image.copyTo(imageHistEq);
        }

        cv::equalizeHist(imageHistEq, imageHistEq);

        std::string title = "HIST EQ Preview of: " + topicName;
        cv::imshow(title.c_str(), imageHistEq);
    }
}