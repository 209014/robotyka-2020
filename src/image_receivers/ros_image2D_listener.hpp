#ifndef ROS_IMAGE2D_LISTENER_HPP
#define ROS_IMAGE2D_LISTENER_HPP

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>

namespace ImageReceivers
{
    class Image2DTopic
    {
    public:
        Image2DTopic(ros::NodeHandle &n, const std::string &topicName);
        void SubscribeToTopic();
        void ImageCallback(const sensor_msgs::ImageConstPtr &msg);
        bool IsNewImageReceived();
        void Display();
        void DisplayHistEqualized(double alpha = 1 / 256.0);
        cv::Mat GetImage();

    private:
        bool newImage = false;
        std::string topicName;
        cv_bridge::CvImagePtr cv_ptr;
        image_transport::Subscriber sub;
        boost::shared_ptr<image_transport::ImageTransport> it;
        cv::Mat image;
    };
}

#endif
