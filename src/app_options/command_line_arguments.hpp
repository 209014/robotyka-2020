#ifndef COMMAND_LINE_ARGUMENTS_HPP
#define COMMAND_LINE_ARGUMENTS_HPP

#include <memory>
#include "program_arguments.hpp"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

namespace AppOptions
{
    class CmdArgumentsReader
    {
    public:
        CmdArgumentsReader(int argc, char **argv);

        bool IsHelpOrUnrecognizedCommandPresent();
        void PrintHelpMessage();
        void Parse(ProgramArguments *programArguments);

    private:
        bool unrecognizedCommandFound = false;
        std::unique_ptr<boost::program_options::options_description> optionsDescription;
        std::unique_ptr<boost::program_options::variables_map> argumentMap;

        void PopulateOptionsDescription();
        void PopulateOptionsValues(ProgramArguments *programArguments);

        void LookForIllegalArgs();
    };
} // namespace AppOptions

#endif
