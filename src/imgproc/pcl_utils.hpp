#ifndef PCL_UTILS_HPP
#define PCL_UTILS_HPP

#include <pcl/pcl_config.h>
#include <pcl/point_cloud.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/console/parse.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>

namespace ImgProc
{
    template <typename PointT>
    double
    ComputeCloudResolution(const typename pcl::PointCloud<PointT>::ConstPtr &cloud);


    pcl::visualization::PCLVisualizer::Ptr
    GetViewer(const std::string &viewerName);

    void
    AddCloudToViewer(const pcl::visualization::PCLVisualizer::Ptr &viewer, pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, const std::string &cloudName);

    void
    AddCloudToViewer(const pcl::visualization::PCLVisualizer::Ptr &viewer, pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud, const std::string &cloudName);

    void
    PrintModelCoefficients(const pcl::ModelCoefficients::Ptr &coefficients, int printFirstN);

    void
    PrintModelInliers(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &cloud, const pcl::PointIndices::Ptr &inliers);

    template <typename PointT>
    pcl::PointCloud<PointT>
    LoadCloudPCDFile(const std::string &path);

    template <typename PointT>
    pcl::SACSegmentation<PointT>
    GetSegmentatorPlaneRANSAC(double distanceThreshold, int maxIterations);

    void
    GenerateSimpleColorShape(pcl::PointCloud<pcl::PointXYZRGB> *cloud);

    pcl::RGB 
    GetEasyDistinguishableColor(int i);
}

#endif
